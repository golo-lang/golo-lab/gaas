const fetch = require('node-fetch');


fetch(`http://localhost:9090/deploy`, {
  method: "POST",
  body: JSON.stringify({
    name: "hello",
    description: "hello world function",
    version: "1.0.0",
    timeout: "100",
    type: "json",
    source: `|params| -> "hello world!"`
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/deploy`, {
  method: "POST",
  body: JSON.stringify({
    name: "hi",
    description: "hi function",
    version: "1.0.0",
    timeout: "100",
    type: "json",
    source: `|params| -> "hi " + params: message()`
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/deploy`, {
  method: "POST",
  body: JSON.stringify({
    name: "add",
    description: "add function",
    version: "1.0.0",
    timeout: "100",
    type: "json",
    source: `|params| -> params: a() + params: b()`
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/deploy`, {
  method: "POST",
  body: JSON.stringify({
    name: "listFiles",
    description: "list files",
    version: "1.0.0",
    timeout: "100",
    type: "json",
    source: `
			|params| {
  			let rt = java.lang.Runtime.getRuntime()
  			let res = rt: exec("ls")
  			return res
			}    
    `
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))


fetch(`http://localhost:9090/deploy`, {
  method: "POST",
  body: JSON.stringify({
    name: "exit",
    description: "exit code",
    version: "1.0.0",
    timeout: "100",
    type: "json",
    source: `
			|params| {
				java.lang.System.exit(0)
  			return null
			}
    `
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/deploy`, {
  method: "POST",
  body: JSON.stringify({
    name: "neverEnd",
    description: "exit code",
    version: "1.0.0",
    timeout: "100",
    type: "json",
    source: `
			|params| {
			  var c = DynamicObject(): value(0)
			  100: times(|i| {
			    c: value(c: value() + 1)
			    sleep(100_L)
			  })
			  return c: value()
			}
    `
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/deploy`, {
  method: "POST",
  body: JSON.stringify({
    name: "divide",
    description: "divide function",
    version: "1.0.0",
    timeout: "100",
    type: "json",
    source: `|params| -> 1/0`
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/deploy`, {
  method: "POST",
  body: JSON.stringify({
    name: "home",
    description: "home page",
    version: "1.0.0",
    timeout: "100",
    type: "html",
    source: `|params| {
      return """
        <h1>👋 Hello 🌍 World!</h1>
      """
    }`
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))


fetch(`http://localhost:9090/deploy`, {
  method: "POST",
  body: JSON.stringify({
    name: "home",
    description: "home page",
    version: "1.0.1",
    timeout: "100",
    type: "html",
    source: `|params| {
      return  "<h1>👋 Hello 🌍 World!</h1><hr><h2>" + params: message()+ "</h2>"
    }`
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))