# GaaS

GaaS or Golo as a Service, is a POC to provide Golo functions as a service.

## Requirements

- install **Redis** server
- `git clone` the project
- run `build.sh` to build jar dependencies
- run `redis-server`
- launch the **GaaS** server: `./go.sh` (by default **GaaS** is listening on `9090`)

If there are persisted functions in the Redis database, there are compiled at start.

- **btw**, you need to install Golo: https://golo-lang.org/

## Deploy functions

See the javascript examples in `deploy.js`

## run functions

See the javascript examples in `run.js`

## TODO

- imports
- discovery (kind of)
- logs (prometheus?)
- delete a function
- create CI samples
- webadmin (vue? or riot?)
- authentication (token for deploy, token for run => optional)

